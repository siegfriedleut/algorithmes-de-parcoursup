/* Copyright 2019 © Ministère de l'Enseignement Supérieur, de la Recherche et de l'Innovation,
    Hugo Gimbert (hugo.gimbert@enseignementsup.gouv.fr) 

    This file is part of Algorithmes-de-parcoursup.

    Algorithmes-de-parcoursup is free software: you can redistribute it and/or modify
    it under the terms of the Affero GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Algorithmes-de-parcoursup is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    Affero GNU General Public License for more details.

    You should have received a copy of the Affero GNU General Public License
    along with Algorithmes-de-parcoursup.  If not, see <http://www.gnu.org/licenses/>.

 */
package parcoursup.propositions.meilleursbacheliers;


public class MeilleurBachelier {

    /* code candidat */
    public final int gCnCod;
    
    /* moyenne au bac */
    public final double moyenne;
    
    public MeilleurBachelier(int gCnCod, double moyenne) {
        this.gCnCod = gCnCod;
        this.moyenne = moyenne;
    }
 
    private MeilleurBachelier() {
        gCnCod = 0;
        moyenne = 0;
    }
}